#include <vector>
#include <iostream>
#include <stdint.h>

// utile en cas d'utilisation d'une bibiliothèque externe
// #include <lo/lo.h>
// #include <lo/lo_cpp.h> 

 int main()
 { 
//	lo::ServerThead ServerLiblo(9000);
        uint32_t taille = 100000000;
        std::vector<uint32_t> vect(taille);
        for (size_t i=0; i<taille;i++)
       		 vect[i] = i;
        std::cout<< "Value: " << vect[taille - 2] << std::endl;
        return 0;
 }

